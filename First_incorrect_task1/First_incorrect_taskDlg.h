
// First_incorrect_taskDlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"


// ���������� ���� CFirst_incorrect_taskDlg
class CFirst_incorrect_taskDlg : public CDialogEx
{
// ��������
public:
	CFirst_incorrect_taskDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_FIRST_INCORRECT_TASK_DIALOG };

	CWnd * PicWnd_Input_Sign;
	CDC * PicDc_Input_Sign;
	CRect Pic_Input_Sign;

	CWnd * PicWnd_Output_Sign;
	CDC * PicDc_Output_Sign;
	CRect Pic_Output_Sign;


	/*CWnd* VosstSignalWnd;
	CDC* VosstSignalDc;
	CRect VosstSignalPic;*/


	CPen koordpen, netkoordpen, signalpen, spectrpen, vsignalpen;
	CPen* pen;
	CFont fontgraph;
	CFont* font;

	CPen osi_pen;		// ��� ���� 
	CPen setka_pen;		// ��� �����
	CPen graf_pen;		// ��� ������� �������
	CPen graf_pen2;
	CPen graf_pen3;

	double
		xx0,
		xxmax,
		yy0,
		yymax,
		xxi,
		yyi,
		iter;

	char
		znach[1000];

	double xp, yp,			//����������� ���������
		xmin, xmax,			//�������������� � ����������� �������� � 
		ymin, ymax;			//�������������� � ����������� �������� y
	
	double xp1, yp1,			//����������� ���������
		xmin1, xmax1,			//�������������� � ����������� �������� � 
		ymin1, ymax1;			//�������������� � ����������� �������� y
	
	double xp2, yp2,			//����������� ���������
		xmin2, xmax2,			//�������������� � ����������� �������� � 
		ymin2, ymax2;			//�������������� � ����������� �������� y

	double xp3, yp3,			//����������� ���������
		xmin3, xmax3,			//�������������� � ����������� �������� � 
		ymin3, ymax3;			//�������������� � ����������� �������� y

	double
		Max,
		Min,
		Mass1Min,
		Mass1Max,
		Mass2Min,
		Mass2Max;

	//void Draw1Graph(float*, CDC*, CRect, CPen*, int, int, CString, CString);
	void Draw2Graph(float*, CPen*, float*, CPen*, CDC*, CRect, float, CString, CString);
	void DrawKoord(CDC*, CRect, CString, CString);

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	// ���������
	double A1;
	double A2;
	double A3;
	// ���������
	double Sigma1;
	double Sigma2;
	double Sigma3;
	// ������ ��������� �������
	double t01;
	double t02;
	double t03;
	//
	int N = 50;
	afx_msg float Input_Signal(int t);
	afx_msg float Impulse_response(int t);
	afx_msg void OnBnClickedButton1();
	double normirovka = 0;
	double max_signal_input = 0;

	float *signal_input = new float[N+1];
	float *impulse_response = new float[N+1];
	float *output_signal = new float[N+1];
	afx_msg float function(float* x);
	afx_msg float MHJ(int kk, float* x);
	float *get_signal_output = new float[N+1];
	afx_msg void Pererisovka();
	afx_msg void OnBnClickedButton2();
	float *kvadr_oshibok = new float[N + 1];
	float* mass_lumbd = new float[N + 1];
	float* xi = new float[N + 1];
	float* h_norm = new float[N];
	//float kvadr_oshib;
	//CButton chek;
	char err[50];
	double d;
	bool killtimer, DrawVosstSignalFlag, DrawVosstSignalKoordFlag;
	int activated;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButton3();
	CEdit Oshibka;
};


