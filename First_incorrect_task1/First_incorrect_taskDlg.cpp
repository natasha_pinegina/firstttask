
// First_incorrect_taskDlg.cpp : ���� ����������
//
#define _USE_MATH_DEFINES
#include <iostream>
#include "stdafx.h"
#include "First_incorrect_task.h"
#include "First_incorrect_taskDlg.h"
#include "afxdialogex.h"
#include <math.h>
#include <iostream>
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


using namespace std;
// ���������� ���� CFirst_incorrect_taskDlg
#define KOORD(x,y) (xp*((x)-xmin)),(yp*((y)-ymax)) 
#define KOORDOUT(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1))

int timer = 0;

DWORD dwThread;
HANDLE hThread;
DWORD WINAPI MyProc(PVOID pv)
{
	CFirst_incorrect_taskDlg* p = (CFirst_incorrect_taskDlg*)pv;
	//p->CalculateVosstSignal();
	p->MHJ(p->N, p->mass_lumbd);
	return 0;
}


CFirst_incorrect_taskDlg::CFirst_incorrect_taskDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFirst_incorrect_taskDlg::IDD, pParent)
	, A1(2)
	, A2(6)
	, A3(3)
	, Sigma1(5)
	, Sigma2(2)
	, Sigma3(4)
	, t01(5)
	, t02(30)
	, t03(45)
	/*, kvadr_oshib(0)*/
	, d(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFirst_incorrect_taskDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT3, A1);
	DDX_Text(pDX, IDC_EDIT2, A2);
	DDX_Text(pDX, IDC_EDIT4, A3);
	DDX_Text(pDX, IDC_EDIT6, Sigma1);
	DDX_Text(pDX, IDC_EDIT5, Sigma2);
	DDX_Text(pDX, IDC_EDIT7, Sigma3);
	DDX_Text(pDX, IDC_EDIT8, t01);
	DDX_Text(pDX, IDC_EDIT9, t02);
	DDX_Text(pDX, IDC_EDIT10, t03);
	/*DDX_Text(pDX, IDC_MJH, kvadr_oshib);*/
	DDX_Text(pDX, IDC_EDIT11, d);
	DDX_Control(pDX, IDC_MJH, Oshibka);
}

BEGIN_MESSAGE_MAP(CFirst_incorrect_taskDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CFirst_incorrect_taskDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CFirst_incorrect_taskDlg::OnBnClickedButton2)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON3, &CFirst_incorrect_taskDlg::OnBnClickedButton3)
END_MESSAGE_MAP()



// ����������� ��������� CFirst_incorrect_taskDlg

BOOL CFirst_incorrect_taskDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	PicWnd_Input_Sign = GetDlgItem(IDC_Input_Signal);
	PicDc_Input_Sign = PicWnd_Input_Sign->GetDC();
	PicWnd_Input_Sign->GetClientRect(&Pic_Input_Sign);

	PicWnd_Output_Sign = GetDlgItem(IDC_Output_Signal);
	PicDc_Output_Sign = PicWnd_Output_Sign->GetDC();
	PicWnd_Output_Sign->GetClientRect(&Pic_Output_Sign);


	setka_pen.CreatePen(		//��� �����
		PS_DOT,					//����������
		0.1,						//������� 1 �������
		RGB(0, 0, 250));		//����  ������
	osi_pen.CreatePen(			//������������ ���
		PS_SOLID,				//�������� �����
		3,						//������� 2 �������
		RGB(0, 0, 0));			//���� ������

	graf_pen.CreatePen(			//������
		PS_SOLID,				//�������� �����
		2,						//������� 2 �������
		RGB(0, 0, 255));			//���� ������
	graf_pen2.CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
	graf_pen3.CreatePen(PS_SOLID, 2, RGB(155, 150, 150));
	activated = 0;
	DrawVosstSignalFlag = false;
	
	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CFirst_incorrect_taskDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CFirst_incorrect_taskDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CFirst_incorrect_taskDlg::Draw2Graph(float* Mass1, CPen* graph1pen, float* Mass2, CPen* graph2pen, CDC* WinDc, CRect WinPic, float AbsMax, CString Abs, CString Ord)
{
	//----- ����� ������������� � ������������ �������� -----------------------------
	Mass1Min = Mass1[0];
	Mass1Max = Mass1[0];
	Mass2Min = Mass2[0];
	Mass2Max = Mass2[0];
	for (int i = 1; i < N; i++)
	{
		if (Mass1[i] < Mass1Min)
		{
			Mass1Min = Mass1[i];
		}
		if (Mass1[i] > Mass1Max)
		{
			Mass1Max = Mass1[i];
		}
		if (Mass2[i] < Mass2Min)
		{
			Mass2Min = Mass2[i];
		}
		if (Mass2[i] > Mass2Max)
		{
			Mass2Max = Mass2[i];
		}
	}
	if (Mass2Max > Mass1Max)
	{
		Max = Mass2Max;
	}
	else
	{
		Max = Mass1Max;
	}
	if (Mass2Min < Mass1Min)
	{
		Min = Mass2Min;
	}
	else
	{
		Min = Mass1Min;
	}
	//---- ��������� -------------------------------------------------
	// �������� ��������� ����������
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	//------ ������� ���� ������� ����� ������ ---------------------------------
	MemDc->FillSolidRect(
		WinPic,
		RGB(
			255,
			255,
			255));
	//------ ��������� ����� ��������� -----------------------------------------
	pen = MemDc->SelectObject(&setka_pen);
	// ������������ ����� ����� ���������
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width(); i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}
	// �������������� ����� ����� ���������
	for (float i = (float)WinPic.Height() / 10; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}
	//------ ��������� ���� ----------------------------------------------------
	pen = MemDc->SelectObject(&koordpen);
	// ��������� ��� X
	MemDc->MoveTo(2, (float)WinPic.Height() * 9 / 10);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 + 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 - 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	// ������� �� ��� X
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width() * 24 / 25; i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, WinPic.Height() * 9 / 10 + 2);
		MemDc->LineTo(i, WinPic.Height() * 9 / 10 - 3);
	}
	// ��������� ��� Y
	MemDc->MoveTo(WinPic.Width() * 2 / 25, WinPic.Height() - 2);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 + 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	// ������� �� ��� Y
	for (float i = (float)WinPic.Height() / 5; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, i);
		MemDc->LineTo(WinPic.Width() * 2 / 25 + 3, i);
	}
	//------ ������� ���� --------------------------------------------
	// ��������� ����������� ���� ������
	MemDc->SetBkMode(TRANSPARENT);
	// ��������� ������
	MemDc->SelectObject(&fontgraph);
	// ������� ��� X
	MemDc->TextOut((float)WinPic.Width() * 24 / 25 + 4, (float)WinPic.Height() * 9 / 10 + 2, Abs);
	// ������� ��� Y
	MemDc->TextOut((float)WinPic.Width() * 2 / 25 + 5, 0, Ord);
	// ����� ������� ��� ���������
	xx0 = WinPic.Width() * 2 / 25;
	xxmax = WinPic.Width() * 24 / 25;
	yy0 = WinPic.Height() / 10;
	yymax = WinPic.Height() * 9 / 10;
	// ��������� ������� �������
	pen = MemDc->SelectObject(graph1pen);
	MemDc->MoveTo(xx0, yymax + (Mass1[0] - Min) / (Max - Min) * (yy0 - yymax));
	for (int i = 0; i < N; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (N - 1);
		yyi = yymax + (Mass1[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	// ��������� ������� �������
	pen = MemDc->SelectObject(graph2pen);
	MemDc->MoveTo(xx0, yymax + (Mass2[0] - Min) / (Max - Min) * (yy0 - yymax));
	for (int i = 0; i < N; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (N - 1);
		yyi = yymax + (Mass2[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	//----- ����� �������� �������� -----------------------------------
	// �� ��� �������
	for (int i = 6; i < 25; i += 5)
	{
		sprintf(znach, "%5.1f", (i - 1) * (float)AbsMax / 22);
		MemDc->TextOut(i * WinPic.Width() / 25 + 2, WinPic.Height() * 9 / 10 + 2, CString(znach));
	}
	// �� ��� �������
	sprintf(znach, "%5.2f", Max);
	MemDc->TextOut(0, WinPic.Height() / 20 + 1, CString(znach));
	sprintf(znach, "%5.2f", 0.75 * Max);
	MemDc->TextOut(0, WinPic.Height() * 5 / 20 + 1, CString(znach));
	sprintf(znach, "%5.2f", 0.5 * Max);
	MemDc->TextOut(0, WinPic.Height() * 9 / 20 + 1, CString(znach));
	sprintf(znach, "%5.2f", 0.25 * Max);
	MemDc->TextOut(0, WinPic.Height() * 13 / 20 + 1, CString(znach));
	sprintf(znach, "%5.2f", 0 * Max);
	MemDc->TextOutW(0, WinPic.Height() * 9 / 10 + 2, CString(znach));
	//----- ����� �� ����� -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

float CFirst_incorrect_taskDlg::Input_Signal(int t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Sigma[] = { Sigma1, Sigma2, Sigma3 };
	double Centers_of_Gaussian_domes[] = { t01, t02, t03 };
	float result = 0;
	for (int i = 0; i <= 2; i++)
	{
		result += Amplitude[i] * exp(-((t - Centers_of_Gaussian_domes[i]) / Sigma[i])*((t - Centers_of_Gaussian_domes[i]) / Sigma[i]));
	}
	return result;
}

float CFirst_incorrect_taskDlg::Impulse_response(int t)
{
	double Sigma[] = {5,5};
	double Centers_of_Gaussian_domes[] = {0,50};
	float result = 0;
	for (int i = 0; i <= 1; i++)
	{
		result +=exp(-((t - Centers_of_Gaussian_domes[i]) / Sigma[i])*((t - Centers_of_Gaussian_domes[i]) / Sigma[i]));
	}
	return result;
}

void CFirst_incorrect_taskDlg::OnBnClickedButton1()
{
	UpdateData(true);
	
	memset(signal_input, 0,(N+1)*sizeof(float));
	memset(impulse_response, 0, (N + 1) * sizeof(float));
	
	double r[13] = { 0 }, t1 = 0, Kol, Summ_Eps = 0;
	double *Eps = new double[(int)N + 1]();
	double *Shum = new double[(int)N + 1]();
	//��������� �����
	for (int j = 0; j <N; j++)
	{
		Kol = 0;
		for (int i = 0; i<13; i++)
		{

			r[i] = 2 * (rand() % 100) / (100 * 1.0) - 1;
			Kol += r[i];
		}
		Eps[j] = Kol / 12;
		Summ_Eps += Eps[j] * Eps[j];
	}
	double Es = 0, En = 0;

	double Amplitude[] = { A1, A2, A3 };
	double Sigma[] = { Sigma1, Sigma2, Sigma3 };
	double Centers_of_Gaussian_domes[] = { t01, t02, t03 };
	double energy = d / 100;
	double s1 = 0, s2 = 0, s3 = 0, s = 0;
	//������� �������
	while (t1 < N)
	{
		s1 = Amplitude[0] * exp(-((t1 - Centers_of_Gaussian_domes[0]) / Sigma[0])*((t1 - Centers_of_Gaussian_domes[0]) / Sigma[0]));
		s2 = Amplitude[1] * exp(-((t1 - Centers_of_Gaussian_domes[1]) / Sigma[1])*((t1 - Centers_of_Gaussian_domes[1]) / Sigma[1]));
		s3 = Amplitude[2] * exp(-((t1 - Centers_of_Gaussian_domes[2]) / Sigma[2])*((t1 - Centers_of_Gaussian_domes[2]) / Sigma[2]));
		s = s1 + s2 + s3;
		t1 += 1;
		Es += s*s;
	}
	//������� ����
	En = energy*Es;
	//���������� ����������� 
	double alpha = sqrt(En / Summ_Eps);
	t1 = 0;
	//��������� ����, ���������� ������� ������+���
	for (int j = 0; t1<N; j++)
	{
		Shum[j] = Eps[j] * alpha;
		t1 += 1;
	}
	normirovka = 0;
	//��������� ������� �������� ������� � ���������� ��������������
	for (int i = 0; i <N; i++)
	{
		impulse_response[i] = Impulse_response(i);
		signal_input[i] = Input_Signal(i);
		normirovka += impulse_response[i];
		if (max_signal_input < signal_input[i])
			max_signal_input = signal_input[i];
	}


	xmin = -2;			//����������� �������� �
	xmax = N;			//������������ �������� �
	ymin = -max_signal_input/10;		//����������� �������� y
	ymax = max_signal_input;//������������ �������� y


	xp = ((double)(Pic_Input_Sign.Width()) / (xmax - xmin));			//������������ ��������� ��������� �� �
	yp = -((double)(Pic_Input_Sign.Height()) / (ymax - ymin));

	PicDc_Input_Sign->FillSolidRect(&Pic_Input_Sign, RGB(250, 250, 250));			//���������� ��� 


	PicDc_Input_Sign->SelectObject(&osi_pen);
	//������ ��� Y
	PicDc_Input_Sign->MoveTo(KOORD(0, ymax));
	PicDc_Input_Sign->LineTo(KOORD(0, ymin));
	//������ ��� �
	PicDc_Input_Sign->MoveTo(KOORD(xmin, 0));
	PicDc_Input_Sign->LineTo(KOORD(xmax, 0));

	//������� ����
	PicDc_Input_Sign->TextOutW(KOORD(0, ymax - 0.2), _T("Y")); //Y
	PicDc_Input_Sign->TextOutW(KOORD(xmax - 0.3, 0 - 0.2), _T("t")); //X

	PicDc_Input_Sign->SelectObject(&setka_pen);

	//��������� ����� �� y
	for (float x = xmin + 1; x <= xmax; x += xmax / 10)
	{
		PicDc_Input_Sign->MoveTo(KOORD(x, ymax));
		PicDc_Input_Sign->LineTo(KOORD(x, ymin));
	}
	//��������� ����� �� x
	for (float y = ymin + 1; y <= ymax; y += ymax / 5)
	{
		PicDc_Input_Sign->MoveTo(KOORD(xmin, y));
		PicDc_Input_Sign->LineTo(KOORD(xmax, y));
	}


	//������� ����� �� ���
	CFont font3;
	font3.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDc_Input_Sign->SelectObject(font3);
	//�� Y � ����� 5
	for (double i = ymin; i <= ymax; i += ymax / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDc_Input_Sign->TextOutW(KOORD(0.2, i), str);
	}
	//�� X � ����� 0.5
	for (double j = xmin; j <= xmax; j += xmax / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDc_Input_Sign->TextOutW(KOORD(j - 0.25, -0.2), str);
	}
	//������� ������� ������
	PicDc_Input_Sign->SelectObject(&graf_pen2);
	PicDc_Input_Sign->MoveTo(KOORD(0, signal_input[0]));;
	for (int i = 0; i <N; i++)
	{
		PicDc_Input_Sign->LineTo(KOORD(i, signal_input[i]));
	}
	
	for (int j = 0; j <N; j++)
	{
		h_norm[j] = impulse_response[j] / normirovka;
	}
	//��������� ������ ��������� �������
	memset(output_signal, 0, (N + 1)*sizeof(float));
	int t =0 ;
	double max_output_signal = 0;
	for (int i = 0; i <N; i++)
	{
		//output_signal[i] = 0;
		for (int k = 0; k <N; k++)
		{ 
			if (i-k<0)
			{
				t = N +(i - k);
				output_signal[i] += signal_input[k] * h_norm[t];
			}
			else
				output_signal[i] += signal_input[k] * h_norm[i - k];
		}
		output_signal[i] = output_signal[i] + Shum[i];
		if (max_output_signal < output_signal[i])
			max_output_signal = output_signal[i];
	}

	FILE*results;
	if (fopen_s(&results, "��������� ��������� ������� �� �������.txt", "w") == 0)
	{
		for (int i = 0; i < N; i++) {
			fprintf(results, "%+*.1f ", 10, output_signal[i]);
			fprintf(results, "\n");
		}
	}

	xmin1 = -2;			//����������� �������� �
	xmax1 = N;			//������������ �������� �
	ymin1 = -max_output_signal / 10-2;		//����������� �������� y
	ymax1 = max_output_signal+2;//������������ �������� y
	//ymin1 = -1;
	//ymax1 = 4;

	xp1 = ((double)(Pic_Output_Sign.Width()) / (xmax1 - xmin1));			//������������ ��������� ��������� �� �
	yp1 = -((double)(Pic_Output_Sign.Height()) / (ymax1 - ymin1));

	PicDc_Output_Sign->FillSolidRect(&Pic_Output_Sign, RGB(250, 250, 250));			//���������� ��� 


	PicDc_Output_Sign->SelectObject(&osi_pen);
	//������ ��� Y
	PicDc_Output_Sign->MoveTo(KOORDOUT(0, ymax1));
	PicDc_Output_Sign->LineTo(KOORDOUT(0, ymin1));
	//������ ��� �
	PicDc_Output_Sign->MoveTo(KOORDOUT(xmin1, 0));
	PicDc_Output_Sign->LineTo(KOORDOUT(xmax1, 0));

	//������� ����
	PicDc_Output_Sign->TextOutW(KOORDOUT(0, ymax1 - 0.2), _T("Y")); //Y
	PicDc_Output_Sign->TextOutW(KOORDOUT(xmax1 - 0.3, 0 - 0.2), _T("t")); //X

	PicDc_Output_Sign->SelectObject(&setka_pen);

	//��������� ����� �� y
	for (float x = xmin1 + 1; x <= xmax1; x += xmax1 / 10)
	{
		PicDc_Output_Sign->MoveTo(KOORDOUT(x, ymax1));
		PicDc_Output_Sign->LineTo(KOORDOUT(x, ymin1));
	}
	//��������� ����� �� x
	for (float y = ymin1 + 1; y <= ymax1; y += ymax / 5)
	{
		PicDc_Output_Sign->MoveTo(KOORDOUT(xmin1, y));
		PicDc_Output_Sign->LineTo(KOORDOUT(xmax1, y));
	}


	//������� ����� �� ���
	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDc_Output_Sign->SelectObject(font3);
	//�� Y � ����� 5
	for (double i = ymin1; i <= ymax1; i += ymax1 / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDc_Output_Sign->TextOutW(KOORDOUT(0.2, i), str);
	}
	//�� X � ����� 0.5
	for (double j = xmin1; j <= xmax1; j += xmax1 / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDc_Output_Sign->TextOutW(KOORDOUT(j - 0.25, -0.2), str);
	}
	//������� �������� ������
	PicDc_Output_Sign->SelectObject(&graf_pen);
	PicDc_Output_Sign->MoveTo(KOORDOUT(0, output_signal[0]));;
	for (int i = 0; i <N; i++)
	{
		PicDc_Output_Sign->LineTo(KOORDOUT(i, output_signal[i]));
	}
	//������� ��������� ��������������
	PicDc_Output_Sign->SelectObject(&graf_pen2);
	PicDc_Output_Sign->MoveTo(KOORDOUT(0, impulse_response[0]));;
	for (int i = 0; i <N; i++)
	{
		PicDc_Output_Sign->LineTo(KOORDOUT(i, impulse_response[i]));
	}
}

//������� ����������� ���� ��� ��������
void CFirst_incorrect_taskDlg::Pererisovka()
{
	xmin = -2;			//����������� �������� �
	xmax = N;			//������������ �������� �
	ymin = -max_signal_input/10;		//����������� �������� y
	ymax = max_signal_input;//������������ �������� y


	xp = ((double)(Pic_Input_Sign.Width()) / (xmax - xmin));			//������������ ��������� ��������� �� �
	yp = -((double)(Pic_Input_Sign.Height()) / (ymax - ymin));

	PicDc_Input_Sign->FillSolidRect(&Pic_Input_Sign, RGB(250, 250, 250));			//���������� ��� 


	PicDc_Input_Sign->SelectObject(&osi_pen);
	//������ ��� Y
	PicDc_Input_Sign->MoveTo(KOORD(0, ymax));
	PicDc_Input_Sign->LineTo(KOORD(0, ymin));
	//������ ��� �
	PicDc_Input_Sign->MoveTo(KOORD(xmin, 0));
	PicDc_Input_Sign->LineTo(KOORD(xmax, 0));

	//������� ����
	PicDc_Input_Sign->TextOutW(KOORD(0, ymax - 0.2), _T("Y")); //Y
	PicDc_Input_Sign->TextOutW(KOORD(xmax - 0.3, 0 - 0.2), _T("t")); //X

	PicDc_Input_Sign->SelectObject(&setka_pen);

	//��������� ����� �� y
	for (float x = xmin + 1; x <= xmax; x += xmax / 10)
	{
		PicDc_Input_Sign->MoveTo(KOORD(x, ymax));
		PicDc_Input_Sign->LineTo(KOORD(x, ymin));
	}
	//��������� ����� �� x
	for (float y = ymin + 1; y <= ymax; y += ymax / 5)
	{
		PicDc_Input_Sign->MoveTo(KOORD(xmin, y));
		PicDc_Input_Sign->LineTo(KOORD(xmax, y));
	}


	//������� ����� �� ���
	CFont font3;
	font3.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDc_Input_Sign->SelectObject(font3);
	//�� Y � ����� 5
	for (double i = ymin; i <= ymax; i += ymax / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDc_Input_Sign->TextOutW(KOORD(0.2, i), str);
	}
	//�� X � ����� 0.5
	for (double j = xmin; j <= xmax; j += xmax / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDc_Input_Sign->TextOutW(KOORD(j - 0.25, -0.2), str);
	}
	
}


float CFirst_incorrect_taskDlg::function(float*x)
{
	// ��������� �������������� �������
	// ���������� �������� �������
	// ���������� ���������� �������� ������ ������, � ��������� ������ �������� ��������� �������
	//	........


	double buff1 = 0;
	for (int i = 0; i < N; i++)
	{
		buff1 = 0;
		for (int k = 0; k < N; k++)
		{
			if (i - k < 0)
			{
				buff1 += x[k] * h_norm[N + i - k];
			}
			else
			{
				buff1 += x[k] * h_norm[i - k];
			}
		}
		xi[i] = exp(-1 - buff1);
	}

	double value = 0;
	double buff2 = 0;
	for (int i = 0; i < N; i++)
	{
		buff2 = 0;
		for (int k = 0; k < N; k++)
		{
			if (i - k < 0)
			{
				buff2 += xi[k] * h_norm[N + i - k];
			}
			else
			{
				buff2 += xi[k] * h_norm[i - k];
			}
		}
		value += (output_signal[i] - buff2) * (output_signal[i] - buff2);
	}
	return value;
}

float CFirst_incorrect_taskDlg::MHJ(int kk, float* x)
{
	// kk - ���������� ����������; x - ������ ����������
double  TAU = 1.e-6f; // �������� ���������� 
int i, j, bs, ps;
double z, h, k, fi, fb;
double* b = new double[kk];
double* y = new double[kk];
double* p = new double[kk];
h = 1;
x[0] = 1;
for (i = 1; i < kk; i++)
{
	x[i] = (double)rand() / RAND_MAX; // �������� ��������� �����������
}

k = h;
for (i = 0; i < kk; i++)
{
	y[i] = p[i] = b[i] = x[i];
}
fi = function(x);
ps = 0;
bs = 1;
fb = fi;
j = 0;
while (1)
{
	x[j] = y[j] + k;
	z = function(x);
	sprintf(err, "%.10f", z);
	Oshibka.SetWindowTextW((CString)err);
	if (z >= fi)
	{
		x[j] = y[j] - k;
		z = function(x);
		if (z < fi)
		{
			y[j] = x[j];
		}
		else  x[j] = y[j];
	}
	else  y[j] = x[j];
	fi = function(x);
	if (j < kk - 1)
	{
		j++;
		continue;
	}
	if (fi + 1e-8 >= fb)
	{
		if (ps == 1 && bs == 0)
		{
			for (i = 0; i < kk; i++)
			{
				p[i] = y[i] = x[i] = b[i];
			}
			z = function(x);
			bs = 1;
			ps = 0;
			fi = z;
			fb = z;
			j = 0;
			continue;
		}
		k /= 10.;
		if (k < TAU)
		{
			break;
		}
		j = 0;
		continue;
	}
	for (i = 0; i < kk; i++)
	{
		p[i] = 2 * y[i] - b[i];
		b[i] = y[i];
		x[i] = p[i];
		y[i] = x[i];
	}
	z = function(x);
	fb = fi;
	ps = 1;
	bs = 0;
	fi = z;
	j = 0;
	
	Invalidate(0);
	Draw2Graph(signal_input, &graf_pen2, xi, &graf_pen3, PicDc_Input_Sign, Pic_Input_Sign, N, CString("x"),
		CString("Ampl"));
	
}
for (i = 0; i < kk; i++)
{
	x[i] = p[i];
}


delete b;
delete y;
delete p;
return fb;
}

void CFirst_incorrect_taskDlg::OnBnClickedButton2()
{
	memset(xi, 0, (N + 1) * sizeof(float));
	memset(mass_lumbd, 0, (N + 1) * sizeof(float));
	/*DrawVosstSignalKoordFlag = false;
	DrawVosstSignalFlag = true;*/
	
	hThread = CreateThread(NULL, 0, MyProc, this, 0, &dwThread);
	timer = SetTimer(1, 1, NULL);
}


void CFirst_incorrect_taskDlg::OnTimer(UINT_PTR nIDEvent)
{

	Invalidate(0);
	if (killtimer)
	{
		KillTimer(timer);
		UpdateData(false);
	}
	
	CDialogEx::OnTimer(nIDEvent);
}


void CFirst_incorrect_taskDlg::OnBnClickedButton3()
{
	switch (activated)
	{
	case 0:
		KillTimer(1);
		GetDlgItem(IDC_BUTTON3)->SetWindowTextW(L"��������� ��������");
		SuspendThread(hThread);
		activated = 1;
		break;
	case 1:
		SetTimer(1, 1, NULL);
		GetDlgItem(IDC_BUTTON3)->SetWindowTextW(L"���������� ��������");
		ResumeThread(hThread);
		activated = 0;
		break;
	}
}
